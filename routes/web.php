<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/welcome', 'EmployeController@index');

    Route::get('/add/company', 'CompanyController@create')->name('company.add');

    Route::post('/company', 'CompanyController@store')->name('company.store');

    Route::get('/view/company', 'CompanyController@view')->name('company.view');

    Route::delete('/company/{id}', 'CompanyController@delete')->name('company.delete');

    Route::get('/company/{id}', 'CompanyController@edit')->name('company.edit');

    Route::patch('/company/{id}', 'CompanyController@update')->name('company.update');

    Route::get('/add/employee', 'EmployeController@create')->name('employee.add');

    Route::post('/product', 'EmployeController@store')->name('employee.store');

    Route::delete('/product/{id}', 'EmployeController@delete')->name('employee.delete');

    Route::get('/product/{id}', 'EmployeController@edit')->name('employee.edit');

    Route::patch('/product/{id}', 'EmployeController@update')->name('employee.update');
});
