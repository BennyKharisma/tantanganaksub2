<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    protected $fillable = ['name', 'position', 'age', 'fee', 'company_id'];

    public function company(){
        return $this->belongsTo('App\Company');
    }
}
