<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{
    public function create(){
        return view('registercompany');
    }

    public function store(Request $request){
        Company::create([
            'name' => $request->company_name
        ]);
        return redirect('/view/company');
    }

    public function view(){
        $company = Company::all();
        return view('viewcompany', compact('company'));
    }

    public function delete($id){
        Company::destroy($id);
        return back();
    }

    public function edit($id){
        $company = Company::findOrFail($id);
        return view('edit', compact('company'));
    }

    public function update(Request $request, $id){
        Company::findOrFail($id)->update([
            'name' => $request->company_name
        ]);
        return redirect('/view/company');
    }
}
