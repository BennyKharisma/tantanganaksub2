<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employe;
use App\Company;

class EmployeController extends Controller
{
    public function index(){
        $employee = Employe::all();
        return view('welcome', compact('employee'));
    }

    public function create(){
        $company = Company::all();
        return view('registeremployee', compact('company'));
    }

    public function store(Request $request){
        Employe::create([
            'company_id' => $request->employee_company,
            'name' => $request->employee_name,
            'position' => $request->employee_position,
            'age' => $request->employee_age,
            'fee' => $request->employee_fee
        ]);
        return redirect('/welcome');
    }

    public function delete($id){
        Employe::destroy($id);
        return back();
    }

    public function edit($id){
        $resign = Employe::findOrFail($id);
        $company = Company::all();
        return view('editcompany', compact('resign','company'));
    }

    public function update(Request $request, $id){
        Employe::findOrFail($id)->update([
            'company_id' => $request->employee_company,
            'name' => $request->employee_name,
            'position' => $request->employee_position,
            'age' => $request->employee_age,
            'fee' => $request->employee_fee
        ]);
        return redirect('/welcome');
    }
}
