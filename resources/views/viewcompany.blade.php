@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
            <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($company as $companies)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$companies->name}}</td>
                <td>
                    <a href="{{route('company.edit', $companies->id)}}">Edit</a>
                    <form action="{{route('company.delete', $companies->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
    </tbody>
    <a href="/add/company">&emsp;Add New Company</a> <br>
    <a href="/welcome">&emsp;View Employee List</a>
@endsection