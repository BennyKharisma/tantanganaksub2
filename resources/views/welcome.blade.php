@extends('layouts.app')

@section('content')
    <table class="table">
        <thead>
            <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Age</th>
            <th scope="col">Company</th>
            <th scope="col">Position</th>
            <th scope="col">Fee</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($employee as $employe)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$employe->name}}</td>
                <td>{{$employe->age}}</td>
                <td>{{$employe->company->name}}</td>
                <td>{{$employe->position}}</td>
                <td>{{$employe->fee}}</td>
                <td>
                    <a href="{{route('employee.edit', $employe->id)}}">Edit</a>
                    <form action="{{route('employee.delete', $employe->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
    </tbody>
    <a href="/add/employee">&emsp;Add New Employee</a> <br>
    <a href="/view/company">&emsp;View Company List</a>
@endsection