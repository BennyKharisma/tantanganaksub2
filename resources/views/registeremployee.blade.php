@extends('layouts.app')

@section('content')
    <form action="{{route('employee.store')}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Nama</label>
            <input type="text" name="employee_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div> <br>
        <div class="form-group">
            <label for="exampleInputPassword1">Age</label>
            <input type="text" name="employee_age" class="form-control" id="exampleInputPassword1">
        </div> <br>
        <div class="form-group">
            <label for="exampleInputEmail1">Company</label>
            <select name="employee_company" class="form-control">
                @foreach ($company as $comp)
                <option value="{{$comp->id}}">{{$comp->name}}</option>
                @endforeach
            </select>
        </div> <br>
        <div class="form-group">
            <label for="exampleInputPassword1">Position</label>
            <input type="text" name="employee_position" class="form-control" id="exampleInputPassword1">
        </div> <br>
        <div class="form-group">
            <label for="exampleInputPassword1">Fee</label>
            <input type="text" name="employee_fee" class="form-control" id="exampleInputPassword1">
        </div> <br>
        <button type="submit" class="btn btn-primary">Add new Employee</button>
    </form> <br>
    <a href="/welcome">Back</a>
@endsection