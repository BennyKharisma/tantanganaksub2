@extends('layouts.app')

@section('content')
    <form action="{{route('company.update', $company->id)}}" method="POST">
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Perusahaan</label>
            <input type="text" name="company_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
            value="{{$company->name}}">
        </div> 
        <button type="submit" class="btn btn-primary">Edit Company</button>
    </form><br>
    <a href="/view/company">Back</a>
@endsection
