@extends('layouts.app')

@section('content')
    <form action="{{route('company.store')}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Nama Perusahaan</label>
            <input type="text" name="company_name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div> 
        <button type="submit" class="btn btn-primary">Add new Company</button>
    </form><br>
    <a href="/welcome">Back</a>
@endsection
